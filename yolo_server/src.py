from yolo import *
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import json
import numpy as np
import model
import os
from flask_cors import CORS, cross_origin
import yolo
import shutil

# load and save model
input_size = (img_h, img_w, channels)
model = yolo_model(input_size)

# LOAD MODEL
model = load_model('./model/text_detect_model.json')
# LOAD WEIGHT
model.load_weights('./model/text_detect_final.h5')

'''
if request.files:
    img = request.files["image"]
    filename = secure_filename(img.filename)
    img.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    img = cv2.imread(UPLOAD_FOLDER + '/' + filename)
    
    predict_func(model, np.expand_dims(img, axis=0), 0.5, 'sample', show=False)
    data["success"] = True
return json.dumps(data, ensure_ascii=False)
'''


def yolo_text_detection(im_path):
    # remove old output
    # create new output
    shutil.rmtree('Result_crop')
    print('remove old output')
    os.mkdir('Result_crop')
    print('create new output')
    img = cv2.imread(im_path)
    img = cv2.resize(img, (512, 512))
    img = (img - 127.5) / 127.5
    yolo.crop_image(model, np.expand_dims(img, axis=0), 0.5, 'sample')
    return result
