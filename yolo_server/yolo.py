from keras import backend as K
import keras
import cv2
from Utils import *
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import Adam
from keras.layers import *
import tensorflow as tf
from tensorflow.keras.applications import MobileNetV3Large
from tensorflow.keras.applications import MobileNetV2
import numpy as np
from keras.models import Model
from keras.models import model_from_json
import os
import numpy as np

# Variable Definition
img_w = 512
img_h = 512
channels = 3
classes = 1
info = 5
grid_w = 16
grid_h = 16


def save_model(model):
    model_json = model.to_json()
    with open("./model/text_detect_model.json", "w") as json_file:
        json_file.write(model_json)


def load_model(strr):
    json_file = open(strr, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    print("LOAD MODEL SUCCESSFUL")
    return loaded_model


def yolo_model(input_shape):
    inp = Input(input_shape)
    model = MobileNetV2(
        input_tensor=inp, include_top=False, weights='imagenet')
    last_layer = model.output
    conv = Conv2D(512, (3, 3), activation=tf.nn.leaky_relu,
                  padding='same')(last_layer)
    conv = Dropout(0.4)(conv)
    bn = BatchNormalization()(conv)
    lr = LeakyReLU(alpha=0.1)(bn)
    conv = Conv2D(128, (3, 3), activation=tf.nn.leaky_relu, padding='same')(lr)
    conv = Dropout(0.4)(conv)
    bn = BatchNormalization()(conv)
    lr = LeakyReLU(alpha=0.1)(bn)
    conv = Conv2D(5, (3, 3), activation=tf.nn.leaky_relu, padding='same')(lr)
    final = Reshape((grid_h, grid_w, classes, info))(conv)

    model = Model(inp, final)

    return model


def predict_func(model, inp, iou, name, show=True):
    ans = model.predict(inp)
    # np.save('Results/ans.npy',ans)
    boxes = decode(ans[0], img_w, img_h, iou)
    img = ((inp + 1) / 2)
    img = img[0]
    for i in boxes:
        i = [int(x) for x in i]
        # coordinates of bounding boxes (i[0] ,i[1]) , (i[2] , i[3])
        img = cv2.rectangle(img, (i[0], i[1]), (i[2], i[3]), color=(0, 255, 0), thickness=2)
    if show:
        plt.imshow(img)
        plt.show()
    cv2.imwrite(os.path.join('Results', str(name) + '.jpg'), img * 255.0)


def crop_image(model, inp, iou, name, show=False):
    print("CROPING....")
    print(inp.shape)
    ans = model.predict(inp)
    boxes = decode(ans[0], img_w, img_h, iou)
    img = ((inp + 1) / 2)
    img = img[0]
    print(boxes)
    for index, i in enumerate(boxes):
        i = [int(x) for x in i]
        crop_img = img[i[1]:i[3], i[0]:i[2]]
        try:
            cv2.imwrite(os.path.join('Result_crop', str(name) + str(index) + '.jpg'), crop_img * 255.0)
        except:
            pass
        print("Crop successfully")
