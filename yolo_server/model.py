from yolo import *
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import json
import numpy as np
import model
import os
from flask_cors import CORS, cross_origin
import src
import base64
import string
import random
import cv2
import shutil
import time

app = Flask(__name__)
UPLOAD_FOLDER = './uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'




@app.route("/", methods=["GET"])
@cross_origin()
def _hello_world():
    return "Hello world"


# region Heakth Check
@app.route('/health', methods=['GET'])
def health_check_handler():
    return json.dumps({"status": "OK"})


@app.route("/predict", methods=["POST"])
@cross_origin()
def predict():
    try:
        status = upload_file_base64_handler()
        # Nếu lưu file thành công:
        if status:
            resutl = src.yolo_text_detection(status)

            return "OK"
        # Nếu lưu file thất bại
        else:
            return json.dumps({"data": ["Error"]})


    except:

        return json.dumps({"data": ["Error"]})



def upload_file_base64_handler():
    try:
        if request.values:
            imfile = request.values['imagebase64']
            name = gen_name()
            url = 'Data/' + name + '.jpg'
            with open(url, "wb") as f:
                f.write(base64.b64decode(imfile))
            return url
        else:
            return false
    except:
        print("Lỗi Upload file base64 ")


def gen_name():
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(32)))
    return result_str


if __name__ == "__main__":
    print("App run!")
    app.run(debug=False, host="0.0.0.0", port=int("8080"), threaded=False)
