import numpy as np
def processing_coord(file,x_sl,y_sl,img_w,img_h,grid_w,grid_h):
    name = open(file , 'r')
    data = name.read().decode("utf-8-sig").encode("utf-8")
    data = data.split("\n")
    data = data[:-1]
    # print(data)
    Y = np.zeros((grid_h,grid_w,1,5))
    i = ','
    for li in data:
        temp_list = []
        file_data = li.split(i)
        strr = file_data[8]
        bb = file_data[:8]
        

        x1 = int(bb[0])*x_sl
        x2 = int(bb[2])*x_sl
        x3 = int(bb[4])*x_sl
        x4 = int(bb[6])*x_sl
       
        y1 = int(bb[1])*y_sl
        y2 = int(bb[3])*y_sl
        y3 = int(bb[5])*y_sl
        y4 = int(bb[7])*y_sl
        
        #te = cv2.rectangle(img,(int(xmin),int(ymin)),(int(xmax),int(ymax)) , color = (0,255,0))
        # print(x1,x2,x3,x4)
        # print(y1,y2,y3,y4)
        w,h,x,y = 0.0, 0.0, 0.0, 0.0
        w = (x2 + x3 - x4 - x1)/(2*img_w)
        h = (y4 + y3 - y2 - y1)/(2*img_h)
        # print('w',(x2 + x3 - x4 - x1))
        # print('h',(y4 + y3 - y2 - y1))
        # print('w,h',w,h)
        x = ((x1+x2+x3+x4)/4)/img_w
        y = ((y1+y2+y3+y4)/4)/img_h
        x = x * grid_w
        y = y * grid_h
        # print('x,y',x,y)

        # print('w',x2-x1)

        Y[int(y),int(x),0,0] = 1
        Y[int(y),int(x),0,1] = x - int(x)
        Y[int(y),int(x),0,2] = y - int(y)
        Y[int(y),int(x),0,3] = w
        Y[int(y),int(x),0,4] = h
    return 1

if __name__ == "__main__":
    grid_h = 16
    grid_w = 16
    img_w = 512
    img_h = 512
    path = '/Users/kiennguyen/ho-so-vin/tai-lieu-khoa-2/CV/Project/ICDAR2013+2015/train_data/gt_img_1.txt' 
    Y = processing_coord(path,1,1,img_w,img_h,grid_w,grid_h)
    print(Y)

