# CV_VIN_Server

Install Anaconda : https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html

## Server Craft
1. open your terminal
2. cd to your dir
```sh
    conda create -n craft python=3.7
    conda activate craft 
    python server.py
```
Your server is ready in  http://localhost:5000/
Check server health : http://localhost:5000/health
## Server using Yolo
1. open your terminal
2. cd to your dir and run 
```sh
    conda create -n yolo python=3.7
    conda activate yolo 
    python model.py
```
 Your server is ready in  http://localhost:8080/
Check server health : http://localhost:8080/health
