import base64
import json
import os
from copy import deepcopy
import time
import cv2
import flask
from flask import request
from flask_cors import CORS


import src

app = flask.Flask(__name__)
CORS(app)


# region Heakth Check
@app.route('/health', methods=['GET'])
def health_check_handler():
    return json.dumps({"status": "OK"})
# endregion

# region API image2text_base64
@app.route('/img2text', methods=['POST'])
def img_2_text_base64_handler():
    start_time = time.time()
    try:
        status = upload_file_base64_handler()
        if status:
            result = src.craft_image(status)
            print('Đã trả kết quả craft_image')
            print("--- %s seconds ---" % (time.time() - start_time))        
            return json.dumps({"data": result})
        else:
            print("--- %s seconds ---" % (time.time() - start_time))
            print('Lỗi nhận dạng')
            return json.dumps({"data": ["Error"]})
    except:
        print('Lỗi  gì đây :(  ')
        print("--- %s seconds ---" % (time.time() - start_time))
        return json.dumps({"data": ["Error"]})

def upload_file_base64_handler():
    try:
        if request.values:
            imfile = request.values['imagebase64']
            name = src.gen_name()
            url = 'Data/' + name + '.jpg'
            with open(url, "wb") as f:
                f.write(base64.b64decode(imfile))
            return url
        else:
            return false
    except:
        print("Lỗi Upload file base64 ")

app.run(host="0.0.0.0", port=int("5000"), debug=False)
