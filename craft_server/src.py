
from craft_text_detector import Craft
import cv2
import random
import string
import matplotlib.pyplot as plt
from PIL import Image
from vietocr.tool.predictor import Predictor
from vietocr.tool.config import Cfg
import cv2
import numpy as np
import os
import shutil
import time



config = Cfg.load_config_from_name('vgg_transformer')
config['weights'] = './transformerocr.pth'
#config['weights'] = 'https://drive.google.com/uc?id=13327Y1tz1ohsm5YZMyXVMPIOjoOA0OaA'
config['cnn']['pretrained']=False
# If you have GPU-Cuda Kernel 
#config['device'] = 'cuda:0'
# If you don't have GPU-Cuda Kernel 
config['device'] = 'cpu'
config['predictor']['beamsearch']=False
detector = Predictor(config)


def craft_image(image ,output_dir='output/'):
    start_time = time.time()
    print('craft start ')
    
    shutil.rmtree(output_dir)
    print('remove old output')
    os.mkdir(output_dir)
    print('create new output')

    img_path = image
    image = cv2.imread(image)
    # If you don't have GPU-Cuda Kernel 
    craft = Craft(output_dir=output_dir, crop_type="poly", cuda=False)
    
    # If you have GPU-Cuda Kernel 
    #craft = Craft(output_dir=output_dir, crop_type="poly", cuda=True)

    prediction_result = craft.detect_text(image)

    # print(prediction_result)
    print('#####################################################')
    print('craft end: '+ str(time.time() - start_time))

    return tran(img_path)


def decode(image):
    import cv2
    (h, w, d) = image.shape
    image = image[0:int(h * 0.4), int(w * 0.6):w]
    qrCodeDetector = cv2.QRCodeDetector()
    decodedText, points, _ = qrCodeDetector.detectAndDecode(image)
    if points is not None:
        nrOfPoints = len(points)
        for i in range(nrOfPoints):
            nextPointIndex = (i + 1) % nrOfPoints
            cv2.line(image, tuple(points[i][0]), tuple(points[nextPointIndex][0]), (255, 0, 0), 5)

        return decodedText
    else:
        return 'NoQRdetect'

def gen_name():
    
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(32)))
    return result_str




def tran(img_path):
    start_time = time.time()
    print('Ocr start')
    final = []
    #dir_out= 'output/'+img_path[5:37]
    files = os.listdir('output/image_crops')
    for file in files:
        file = './output/image_crops/'+file
        img = Image.open(file)
        #print(file)
        s = detector.predict(img)
        final.append(s)
    print('OCR end: '+ str(time.time() - start_time))
    return final

        